package com.amazonaws.samples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/* Program which takes configFile, sourceBucket & targetBucket as input. configFile which contains directory in each line will be copied from sourceBucket
 * to the targetBucket in S3
 */
public class MoveObjectsFromS3Bucket {
	static String bucketFrom = "sourcebucket";
	static String bucketTo = "targetbucket";
	
	public static void main(String[] args) throws IOException {
		
		if(args.length<3) {
			 System.out.println("3 arguments are needed for the program to run: inputFile sourceBucket targetBucket.");
			 System.exit(1);
		}

		bucketFrom = args[1];
		bucketTo = args[2];		

        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location and is in valid format.",
                    e);
        }

        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion("us-west-1")
            .build();

    	if(!doesBucketExist(bucketFrom,s3)) {
    		System.out.println("The bucket '"+bucketFrom+"' does not exist");
			System.exit(1);
    	}
   	
    	if(!doesBucketExist(bucketTo,s3)) {
    		System.out.println("The bucket '"+bucketTo+"' does not exist");
			System.exit(1);
    	}

        try {
        	
        	List<String> folders = new ArrayList<String>();        	
        	BufferedReader br = null;
        	
        	try {
        		File file = new File(args[0]);
        		br = new BufferedReader(new FileReader(file));        	
        	}catch(Exception e) {
        		System.out.println("Error in reading file:'"+args[0]+"'.  Please check and enter a a valid file");
      			System.exit(1);
        	}
        	
      	  String st;
      	  //add contents of file into an array
      	  while ((st = br.readLine()) != null) {      	    
      	  	folders.add(st);      	    
      	  }
        	 
      	  //each line represents each folder, hence for each folder, copy all of its contents of it to the destination bucket
      	  for (String eachFolder: folders) {      		  
            ListObjectsV2Result result = s3.listObjectsV2(bucketFrom);                        
            List<S3ObjectSummary> objects = result.getObjectSummaries();            
            for (S3ObjectSummary os: objects) {
                if(os.getKey().startsWith(eachFolder)) {//copy all contents of 'eachFolder'                	
                	s3.copyObject(bucketFrom,os.getKey() , bucketTo	, os.getKey());     
                	System.out.println("'"+os.getKey() +"' item is copied over");
                }
            }          
      	  }
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }
    

	public static boolean doesBucketExist(String bucketName,AmazonS3 s3 ) {
		boolean exists = false;
		 try {	            
	            exists = s3.doesBucketExistV2(bucketName);            	            	            
	        } catch (AmazonServiceException ase) {	           
	            return false;
	        } catch (AmazonClientException ace) {	           
	            return false;
	        }
		 return exists;
	}
}
