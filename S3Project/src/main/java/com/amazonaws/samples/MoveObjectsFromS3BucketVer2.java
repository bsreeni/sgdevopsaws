package com.amazonaws.samples;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/* Program which takes configFile, sourceBucket & targetBucket as input. configFile which contains directory in each line will be copied from sourceBucket
 * to the targetBucket in S3
 */
public class MoveObjectsFromS3BucketVer2 {
	public static void main(String[] args) throws IOException {
		
		if(args.length<1) {
			 System.out.println("Parameter is needed to run: propertiesFile.");
			 System.exit(1);
		}

		Properties prop = loadProperties(args[0]);
		if(prop==null) {
			 System.out.println("Not able to load properties file. Program will terminate.");
			 System.exit(1);
		}		
		
		String bucketSource = prop.getProperty("Source-bucket-name");
		String bucketDestination = prop.getProperty("Destination-bucket-name");
		String regionSource = prop.getProperty("Source-bucket-region");
		String regionDestination = prop.getProperty("Destination-bucket-region");
		String folderPathSource = prop.getProperty("Source-folder-path");
		String folderPathDestination = prop.getProperty("Destination-folder-path");
		String accessKey = prop.getProperty("Aws.access.key");
		String accessSecret = prop.getProperty("Aws.access.secret");
		String foldersToMove = prop.getProperty("Folders-to-move");
		String deleteAfterCopy	= prop.getProperty("Delete-after-copy");
		
		System.out.println("---Here are the properties---");
		System.out.println("bucketSource:          "+bucketSource);
		System.out.println("bucketDestination:     "+bucketDestination);
		System.out.println("regionSource:          "+regionSource);
		System.out.println("regionDestination:     "+regionDestination);
		System.out.println("folderPathSource:      "+folderPathSource);
		System.out.println("folderPathDestination: "+folderPathDestination);
		System.out.println("accessKey:             "+accessKey);
		System.out.println("accessSecret:          "+accessSecret);
		System.out.println("foldersToMove:         "+foldersToMove);		
		System.out.println("deleteAfterCopy:       "+deleteAfterCopy);
		
		System.out.println("---Done with extracting properties---");
		
		String[] folders = foldersToMove.split(",");		
		
		BasicAWSCredentials awsCreds = null;
        try {
        	awsCreds = new BasicAWSCredentials(accessKey, accessSecret);
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location and is in valid format.",
                    e);
        }

        //Creating S3 source
        AmazonS3 s3Source = AmazonS3ClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
            .withRegion(regionSource)
            .build();
        
        //Creating S3 Destination
        AmazonS3 s3Destination = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(regionDestination)
                .build();

        System.out.println("Validating source bucket");
    	if(!doesBucketExist(bucketSource,s3Source)) {
    		System.out.println("The bucket '"+bucketSource+"' does not exist");
			System.exit(1);
    	}
    	System.out.println("Validating destination bucket");
    	if(!doesBucketExist(bucketDestination,s3Destination)) {
    		System.out.println("The bucket '"+bucketDestination+"' does not exist");
			System.exit(1);
    	}

        try {        	
        	System.out.println("Validating source directory:"+folderPathSource);
        	if(!doesDirectoryExistInS3Bucket(folderPathSource,s3Source,bucketSource)) {
        		System.out.println("The folder '"+folderPathSource+"' does not exist in bucket:'"+bucketSource+" in the S3:"+s3Source);
    			System.exit(1);
        	}
        
        	System.out.println("Validating destination directory:"+folderPathDestination);
        	if(!doesDirectoryExistInS3Bucket(folderPathDestination,s3Destination,bucketDestination)) {
        		System.out.println("The folder '"+folderPathDestination+"' does not exist in bucket:'"+bucketDestination+" in the S3:"+s3Destination);
    			System.exit(1);
        	}
          System.out.flush();
          System.out.println("----Starting to copy folders----");
          List<String> toBeDeletedKeys = new ArrayList<String>();
      	  //each line represents each folder, hence for each folder, copy all of its contents of it to the destination bucket
      	  for (String eachFolder: folders) {           		
      		 if(!folderPathSource.endsWith("/"))
      			folderPathSource = folderPathSource +"/";
      		 
      		if(!folderPathDestination.endsWith("/"))
      			folderPathDestination = folderPathDestination+"/";
      		
      		String fullFolderPath = folderPathSource+eachFolder;
      		
            ListObjectsV2Result result = s3Source.listObjectsV2(bucketSource);                        
            List<S3ObjectSummary> objects = result.getObjectSummaries();            
            for (S3ObjectSummary os: objects) {
                if(os.getKey().startsWith(fullFolderPath)) {
                	String[] parts = os.getKey().split(folderPathSource,2);
                	String contentPath = parts[1];

                	s3Destination.copyObject(bucketSource,os.getKey() , bucketDestination, folderPathDestination+contentPath);
                	toBeDeletedKeys.add(os.getKey());
                	System.out.println("'"+contentPath +"' item is copied over to: "+folderPathDestination+contentPath);
                }
            }          
      	  }
      	System.out.println("-----DONE with copying the folders-----");    	
      	
    	if(getDeleteAfterCopy(deleteAfterCopy)) {
    		System.out.println("-----Deleting the folders now-----");
      		for(String eachKeyToBeDeleted:toBeDeletedKeys) 
      			s3Source.deleteObject(new DeleteObjectRequest(bucketSource, eachKeyToBeDeleted));
      	}
        	
        System.out.println("-----DONE-----");
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }      
    }
    

	public static boolean doesBucketExist(String bucketName,AmazonS3 s3 ) {
		boolean exists = false;
		 try {	            
	            exists = s3.doesBucketExistV2(bucketName);            	            	            
	        } catch (AmazonServiceException ase) {	           
	            return false;
	        } catch (AmazonClientException ace) {	           
	            return false;
	        }
		 return exists;
	}
	
	public static boolean doesDirectoryExistInS3Bucket(String dirName, AmazonS3 s3, String bucket) {
		  try {
	            ListObjectsV2Result result = s3.listObjectsV2(bucket);                        
	            List<S3ObjectSummary> objects = result.getObjectSummaries();            
	            for (S3ObjectSummary os: objects) {
	                if(os.getKey().startsWith(dirName)) {                		                	
	                	//System.out.println("'"+os.getKey() +"' found");
	                	return true;
	                }
	            }          	      	  
	        } catch (AmazonServiceException ase) {
	            System.out.println("Caught an AmazonServiceException, which means your request made it "
	                    + "to Amazon S3, but was rejected with an error response for some reason.");
	            System.out.println("Error Message:    " + ase.getMessage());	            
	            System.out.println("AWS Error Code:   " + ase.getErrorCode());
	            
	        } catch (AmazonClientException ace) {
	            System.out.println("Caught an AmazonClientException, which means the client encountered "
	                    + "a serious internal problem while trying to communicate with S3, "
	                    + "such as not being able to access the network.");
	            System.out.println("Error Message: " + ace.getMessage());
	        }
		  return false;
	}
	
    public static Properties loadProperties(String propFilePath) {
    	Properties prop = null;
    	InputStream input = null;

    	try {
    		prop = new Properties();
    		input = new FileInputStream(propFilePath);

    		// load a properties file
    		prop.load(input);

    		// get the property value and print it out
    		Set set=prop.entrySet();      		  
    		Iterator itr=set.iterator();  
    		while(itr.hasNext()){  
    			Map.Entry entry=(Map.Entry)itr.next();  
    			//System.out.println(entry.getKey()+" = "+entry.getValue());  
    		}  
    	} catch (IOException ex) {
    		ex.printStackTrace();
    		return null;
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return prop;
    }
    
    public static boolean getDeleteAfterCopy(String deleteAfterCopy) {
    	try {
    		  return new Boolean(deleteAfterCopy).booleanValue();
    	}catch(Exception e) {
    		return false;
    	}
    }
}
