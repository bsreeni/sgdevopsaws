package com.amazonaws.samples;

import java.io.IOException;

public class Split {
	public static void main(String[] args) throws IOException {
		String x = "testfolder1/sourceSecondFolder/sourceThirdFolder/sourceFolder3File1.txt";
		String y = "testfolder1/sourceSecondFolder/";
		
		String z = x.substring(x.indexOf(y));
		String[] parts = x.split(y, 2);
		System.out.println("index="+x.indexOf(y));
		System.out.println("parts="+parts[0]+":"+parts[1]);
	}
}
