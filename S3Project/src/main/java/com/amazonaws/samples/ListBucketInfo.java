package com.amazonaws.samples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class ListBucketInfo {
    public static void main(String[] args) throws IOException {

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (C:\\Users\\sreenivasa.baddam\\.aws\\credentials).
         */
        AWSCredentials credentials = null;
        /*try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
            System.out.println("access:"+credentials.getAWSAccessKeyId());
            System.out.println("secretykey:"+credentials.getAWSSecretKey());
            
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (C:\\Users\\sreenivasa.baddam\\.aws\\credentials), and is in valid format.",
                    e);
        }*/

        /*AmazonS3 s3 = AmazonS3ClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion("us-west-1")
            .build();
		*/
        //BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIT2HRDRLXYUMTBMA", ""); 
        BasicAWSCredentials awsCreds = null;
        try {
        	awsCreds = new BasicAWSCredentials("accessKey", "secretKey");
        	
        }catch (AmazonServiceException ase) {
            System.out.println("Authentication exception: Not able to authenticate the user");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        }
        System.out.println("111");
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion("us-east-1")
                .build();
        System.out.println("2222");
        try {
        	
        	//System.out.println("owner:"+s3.getS3AccountOwner());
        	
        	List<Bucket> buckets = s3.listBuckets();
        	for(Bucket each:buckets)
        		System.out.println("eachB:"+each.toString());
        	
        	
            ListObjectsV2Result result = s3.listObjectsV2("uim-test2");
            System.out.println("333");
            List<S3ObjectSummary> objects = result.getObjectSummaries();
            System.out.println("s1 Info="+objects);
            for (S3ObjectSummary os: objects) {
                System.out.println("eachKey: " + os.getKey());
                
            }
            
//            ListObjectsV2Result result2 = s3.listObjectsV2("sreenibucket2");            
//            
//             objects = result2.getObjectSummaries();
//            System.out.println("s2 Info="+objects);
//            for (S3ObjectSummary os: objects) {
//                System.out.println("* " + os.getKey());
//            }
            
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }               
    }

    /**
     * Creates a temporary file with text data to demonstrate uploading a file
     * to Amazon S3
     *
     * @return A newly created temporary file with text data.
     *
     * @throws IOException
     */
    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("MoveS3DirInfo", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("testfolder1");
        //writer.write("01234567890112345678901234\n");
        //writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        //writer.write("01234567890112345678901234\n");
        //writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.close();

        return file;
    }

    /**
     * Displays the contents of the specified input stream as text.
     *
     * @param input
     *            The input stream to display as text.
     *
     * @throws IOException
     */
    private static void displayTextInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;

            System.out.println("    " + line);
        }
        System.out.println();
    }

}
