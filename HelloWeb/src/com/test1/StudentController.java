package com.test1;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.test1.Pojo.Student;

import org.springframework.ui.ModelMap;

@Controller
public class StudentController {

	@RequestMapping(value="/student", method=RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("student", "command",new Student());
	}
	
	@RequestMapping(value="/addStudent", method=RequestMethod.POST)
	public String addStudent(@ModelAttribute("SpringWeb")Student student,ModelMap model, HttpServletRequest request) {
		model.addAttribute("name",student.getName());
		model.addAttribute("age",student.getAge());
		model.addAttribute("id",student.getId());
		
		for(String key : request.getParameterMap().keySet()){
	        System.out.println(key  + ":=" + request.getParameterMap().get(key));
	    }
		
		return "result";
	}

	/*
	 * public String addStudent(@ModelAttribute Student student, HttpServletRequest request){
    for(String key : request.getParameterMap().keySet()){
        System.out.println(key  + "=" + request.getParameterMap().get(key);
    }
}
	 */
	@RequestMapping(value="/test", method=RequestMethod.POST)
	public String testing() {
		return "test";
	}
}
